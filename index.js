// Bài 1
function timSoNguyenDuong() {
  var sum = 0;
  var i = 1;
  for (i; i < 200; i++) {
    sum += i;
    if (sum > 10000) break;
  }
  //   console.log("i = ", i);
  document.getElementById(
    "result1"
  ).innerText = `Số nguyên dương nhỏ nhất là: ${i}`;
}
timSoNguyenDuong();

// Bài2

function tinhTong() {
  var x = document.getElementById("txt_x").value * 1;
  var n = document.getElementById("txt_n").value * 1;
  var sum = 0;

  for (i = 1; i <= n; i++) {
    sum += Math.pow(x, i);
  }
  console.log("sum: ", sum);

  document.getElementById("result2").innerText = ` Tổng: = ${sum}`;
}

// Bài 3

function tinhGiaiThua() {
  var n = document.getElementById("txt_n_bai3").value * 1;
  var gt = 1;
  for (var i = 1; i <= n; i++) {
    gt *= i;
  }

  document.getElementById("result3").innerText = ` Giai thừa(${n}) = ${gt}`;
}

// Bài 4

function inDiv() {
  var contentHTML = "";

  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      contentHTML += `<div class="p-3 mb-2 bg-danger text-white">Div ${i} Chẵn</div>`;
    } else {
      contentHTML += `<div class="p-3 mb-2 bg-success text-white">Div ${i} Lẽ</div>`;
    }
  }

  document.getElementById("result4").innerHTML = contentHTML;
}
